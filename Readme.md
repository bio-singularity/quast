# Quast 5 singularity recipe

A Singularity recipe to create an image which wrap Quast, the quality assembly tool.

We use the quast software v5.0.0 (latest stable release [Sep 2018]).

## Use it

### Build the image

```bash
	sudo singularity build quast.simg Singularity
```

### Get the image from repositories

Will be available soon on singularity-hub. We are waiting for a compatibility with gitlab.

### launch canu

```bash
	./quast.simg <quast parameters>
```

## Links

* [Quast publication](https://academic.oup.com/bioinformatics/article/34/13/i142/5045727)
* [Quast repository](https://sourceforge.net/projects/quast/)
* [Singularity](https://www.sylabs.io/)
